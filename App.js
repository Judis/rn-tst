import React from 'react';
import { Constants } from 'expo';
import { Button, StyleSheet, Text, View, ScrollView, StatusBar, Image, TouchableOpacity } from 'react-native';
import { DrawerNavigator, SafeAreaView } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

class Banner extends React.Component {
  openDrawer() {
    this.props.navigation.navigate('DrawerOpen');
  }

  render() {
    const openDrawer = this.openDrawer.bind(this);

    return (
      <SafeAreaView
        style={bannerStyles.bannerContainer}
        forceInset={{ vertical: 'never' }}
      >
        <View style={bannerStyles.banner}>
          <View style={bannerStyles.button}>
            <TouchableOpacity onPress={openDrawer}
            >
              <Icon
                name="bars"
                size={24}
                style={bannerStyles.menuButton}
              />
            </TouchableOpacity>
          </View>
          <View style={bannerStyles.header}>
            <Text style={bannerStyles.title}>{this.props.title}</Text>
          </View>
          <View style={bannerStyles.button}></View>
        </View>
        <View style={styles.statusBarUnderlay} />
      </SafeAreaView>
    );
  }
}

const bannerStyles = StyleSheet.create({
  bannerContainer: {
    backgroundColor: '#673ab7',
    paddingTop: Constants.statusBarHeight,
  },
  banner: {
    flexDirection: 'row',
  },
  title: {
    fontSize: 18,
    fontWeight: '200',
    color: '#fff',
    margin: 0,
  },
  header: {
    flex: 1,
    alignItems: 'center',
    padding: 8,
  },
  button: {
    width: 38,
    alignItems: 'center',
    padding: 8,
  },
  menuButton: {
    color: '#fff'
  },
  statusBarUnderlay: {
    backgroundColor: '#673ab7',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: Constants.statusBarHeight,
  },
});

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home'
  };

  changeView() {
    this.props.navigation.navigate('Second');
  }

  render() {
    let changeView = this.changeView.bind(this);

    return (
      <View>
        <Banner navigation={this.props.navigation} title="Issues" />
        <ScrollView>
          <SafeAreaView forceInset={{ top: 'always' }}>
            <Button
              onPress={changeView}
              title="Go To Second View"
            />
          </SafeAreaView>
          <StatusBar barStyle="light-content" />
        </ScrollView>
      </View>
    );
  }
}
HomeScreen.navigationOptions = {
  drawerLabel: 'Issues',
  drawerIcon: ({ tintColor }) => (
    <Icon name="check-circle" size={24} style={{ color: tintColor }} />
  ),
};

class MiniBarScreen extends React.Component {
  changeView(view) {
    this.props.navigation.navigate('Home');
  }

  render() {
    let changeView = this.changeView.bind(this);

    return (
      <ScrollView>
        <SafeAreaView forceInset={{ top: 'always' }}>
        <Banner navigation={this.props.navigation} title="Mini Bar Tracking" />
          <Button
            onPress={changeView}
            title="Go To Home View"
          />
        </SafeAreaView>
        <StatusBar barStyle="light-content" />
      </ScrollView>
    );
  }
}
MiniBarScreen.navigationOptions = {
  drawerLabel: 'Mini Bar Tracking',
  drawerIcon: ({ tintColor }) => (
    <Icon name="glass" size={24} style={{ color: tintColor }} />
  ),
};

class CreateIssueScreen extends React.Component {
  changeView(view) {
    this.props.navigation.navigate('Home');
  }

  render() {
    let changeView = this.changeView.bind(this);

    return (
      <ScrollView>
        <SafeAreaView forceInset={{ top: 'always' }}>
        <Banner navigation={this.props.navigation} title="Create Issue" />
          <Button
            onPress={changeView}
            title="Go To Home View"
          />
        </SafeAreaView>
        <StatusBar barStyle="light-content" />
      </ScrollView>
    );
  }
}
CreateIssueScreen.navigationOptions = {
  drawerLabel: 'Create Issue',
  drawerIcon: ({ tintColor }) => (
    <Icon name="plus" size={24} style={{ color: tintColor }} />
  ),
};

class LostAndFoundScreen extends React.Component {
  changeView(view) {
    this.props.navigation.navigate('Home');
  }

  render() {
    let changeView = this.changeView.bind(this);

    return (
      <ScrollView>
        <SafeAreaView forceInset={{ top: 'always' }}>
        <Banner navigation={this.props.navigation} title="Lost And Found" />
          <Button
            onPress={changeView}
            title="Go To Home View"
          />
        </SafeAreaView>
        <StatusBar barStyle="light-content" />
      </ScrollView>
    );
  }
}
LostAndFoundScreen.navigationOptions = {
  drawerLabel: 'Lost And Found',
  drawerIcon: ({ tintColor }) => (
    <Icon name="question-circle" size={24} style={{ color: tintColor }} />
  ),
};

class ChatScreen extends React.Component {
  changeView(view) {
    this.props.navigation.navigate('Home');
  }

  render() {
    let changeView = this.changeView.bind(this);

    return (
      <ScrollView>
        <SafeAreaView forceInset={{ top: 'always' }}>
        <Banner navigation={this.props.navigation} title="Chat" />
          <Button
            onPress={changeView}
            title="Go To Home View"
          />
        </SafeAreaView>
        <StatusBar barStyle="light-content" />
      </ScrollView>
    );
  }
}
ChatScreen.navigationOptions = {
  drawerLabel: 'Chat',
  drawerIcon: ({ tintColor }) => (
    <Icon name="comments" size={24} style={{ color: tintColor }} />
  ),
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default DrawerNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    MiniBar: {
      screen: MiniBarScreen
    },
    CreateIssue: {
      screen: CreateIssueScreen
    },
    LostAndFound: {
      screen: LostAndFoundScreen
    },
    Chat: {
      screen: ChatScreen
    }
  }, {
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    initialRouteName: 'Home',
    contentOptions: {
      activeTintColor: '#e91e63',
    },
  }
);
